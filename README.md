# Projeto criado para o evento "Implementando AngularJS do Zero" do GDG Juiz de Fora. #

O objetivo deste foi não utilizar nada além do necessário para conseguir focar em uma implementação básica e sem o auxílio de ferramenta.

## Vídeo do Evento ##

[https://www.youtube.com/watch?v=rV03SdUNXPo](https://www.youtube.com/watch?v=rV03SdUNXPo)

![11667353_868650719878629_7022320233753068972_n.png](https://bitbucket.org/repo/7pyxGb/images/3260088840-11667353_868650719878629_7022320233753068972_n.png)