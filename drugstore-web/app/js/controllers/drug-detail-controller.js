app.controller('DrugDetailController', ['$scope', '$modalInstance', 'selectedDrug', function ($scope, $modalInstance, selectedDrug) {	
	$scope.selectedDrug = selectedDrug;

	$scope.close = function ()	{
		$modalInstance.dismiss('cancel');
	};
}]);