app.controller('DrugController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {	
	$scope.drugs = [];

	var onFindAllSuccess = function (drugs) {
		$scope.drugs = drugs;		
	};

	var findAll = function () {
		$http.get('http://localhost:8080/drugs')
		     .success(onFindAllSuccess);
	};

	var init = function () {
		findAll();
	};	

	$scope.onItemClick = function (drug) {
		var selectedDrug = function () {
			return drug;
		};
		$modal.open({animation: true, templateUrl: 'details.html', controller: 'DrugDetailController', resolve: {selectedDrug: selectedDrug}});
	};	

	init();
}]);